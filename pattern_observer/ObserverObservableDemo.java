package ex_pattern_observer_solution;

class ObserverObservableDemo {
    
    public static void main(String args[]) {
        YoutubeChannel i_3_news = new YoutubeChannel();
        AdminObserver e_c = new AdminObserver();
        i_3_news.addObserver(e_c);
        i_3_news.news();
        FirstFollower q_s = new FirstFollower();
        SecondFollower s_b = new SecondFollower();
        i_3_news.addObserver(s_b);
        i_3_news.addObserver(q_s);
        i_3_news.news();

	//notifications dans l'ordre inverse des ajouts a la liste des observateurs
    }
}
