// ALTERNATIVE
public interface HouseBuilderWithReturn {

    public HouseBuilderWithReturn buildStructure();

    public HouseBuilderWithReturn buildInterior();

    public House getHouse();
}
