public class House {
    private String structure;
    private String interior;

    public void setStructure(String s) {
        structure = s;
    }

    public void setInterior(String i) {
        interior = i;
    }

    @Override
    public String toString() {
        return "Structure: " + structure + ", Interior : " + interior;
    }
}
