public class CaravanCivilEngineer {
    private HouseBuilder houseBuilder;

    public CaravanCivilEngineer(HouseBuilder hb) {
        houseBuilder = hb;
    }

    public House getHouse() {
        return houseBuilder.getHouse();
    }

    public void constructHouse() {
        // TODO: construct the house (basement+interior)
        houseBuilder.buildStructure();
        houseBuilder.buildInterior();;
    }
}
