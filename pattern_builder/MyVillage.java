public class MyVillage {
    public static void main(String[] args) {
        // you have to create a very small village
        // use the pattern builder to create a chalet and a caravan
        // use the alternative of the pattern to create an igloo

        // 1. Create builders
        HouseBuilder cb = new CaravanBuilder();
        HouseBuilderWithReturn ib = new IglooBuilder();
        HouseBuilder chaletb = new ChaletBuilder();
        // 2. Create engineers
        CaravanCivilEngineer dirCaravan = new CaravanCivilEngineer(cb);
        IglooCivilEngineer dirIgloo = new IglooCivilEngineer(ib);
        CaravanCivilEngineer dirChalet = new CaravanCivilEngineer(chaletb);
        // 3. Construct and print
        dirCaravan.constructHouse();
        dirIgloo.constructHouse();
        dirChalet.constructHouse();
        
        System.out.println(dirCaravan.getHouse());
        System.out.println(dirIgloo.getHouse());
        System.out.println(dirChalet.getHouse());

        /* Example of output:

        Structure: A caravan, Interior : Basic
        Structure: An igloo, Interior : Frozen
        Structure: A chalet, Interior : Cozy

         */

    }
}
