// ALTERNATIVE
public class IglooCivilEngineer {
    private HouseBuilderWithReturn houseBuilder;

    public IglooCivilEngineer(HouseBuilderWithReturn hb) {
        houseBuilder = hb;
    }

    public House getHouse() {
        return houseBuilder.getHouse();
    }

    public void constructHouse() {
        // TODO: construct the house (basement+interior)
        houseBuilder.buildStructure();
        houseBuilder.buildInterior();
    }
}
