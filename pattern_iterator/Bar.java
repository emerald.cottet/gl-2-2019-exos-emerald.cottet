public class Bar {

    private Menu menu;
    private Drinks drinks;
    private Customers customers;

    public Bar() {
        menu = new Menu();
        drinks = new Drinks();
        customers = new Customers();
    }

    /*
     * Ce programme ne fait rien de bien intelligent. On remarque par contre
     * l'avantage du pattern. L'itération se fait de manière générique peu importe
     * le type de collection utilisé en arrière-plan.
     */
    public void openForBusiness() {
        menu.iterator().forEachRemaining(System.out::println);
        drinks.iterator().forEachRemaining(System.out::println);
        customers.iterator().forEachRemaining(System.out::println);
    }

    public static void main(String[] args) {
        Bar cyclo = new Bar();
        cyclo.openForBusiness();
    }
}