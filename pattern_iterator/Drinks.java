import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Drinks implements Iterable/* TODO: implements ...complétez... */ {

    List<MenuItem> items;

    /*
     * TODO: Complètez la classe DrinksIterator. Si vous estimez qu'il n'y a pas
     * besoins de cette classe, vous êtes libres de faire l'implémentation d'une
     * autre manière.
     */
    private static class DrinksIterator implements Iterator<MenuItem>/* implements ...complétez... */ {

        private Drinks d;
        private int index;
        
        DrinksIterator(Drinks m) {
            this.d = m;
        }
        
        @Override
        public boolean hasNext() {
            // TODO Auto-generated method stub
            if(index < d.items.size())return true;
            return false;
        }

        @Override
        public MenuItem next() {
            // TODO Auto-generated method stub
            if(this.hasNext()){
                return d.items.get(index++);
             }
            return null;
        }

    }

    public Drinks() {
        this.items = new ArrayList<>();
        this.items.add(new MenuItem("Bière blonde", 5.0f));
        this.items.add(new MenuItem("Bière blanche", 6.0f));
        this.items.add(new MenuItem("Bière IPA", 6.5f));
        this.items.add(new MenuItem("Pastis", 5.0f));
    }

    public Iterator iterator() {
        // TODO Auto-generated method stub
        return new DrinksIterator(this);
    }

    /* TODO: ...complétez... */

}