import java.util.Iterator;

public class Menu implements Iterable /* TODO: implements ...complétez... */ {

    private final MenuItem[] items = new MenuItem[] { 
        new MenuItem("Fribourger", 13.20f),
        new MenuItem("Andalous", 14),
        new MenuItem("Salade", 10.50f)
    };

    public static class MenuIterator implements Iterator<MenuItem> /* TODO: implements ...complétez... */ {

        private MenuItem[] menu;
        private int index;

        MenuIterator(MenuItem[] m) {
            this.menu = m;
        }

        @Override
        public boolean hasNext() {
            /* TODO: ...complétez... */
            if(index < menu.length)return true;
            return false;
        }

        @Override
        public MenuItem next() {
            /* TODO: ...complétez... */
            if(this.hasNext()){
                return menu[index++];
             }
            return null;
        }

    }

    public Menu() {
    }
    

    @Override
    public Iterator iterator() {
        // TODO Auto-generated method stub
        return new MenuIterator(items);
    }
    

    /* TODO: ...complétez... */

}