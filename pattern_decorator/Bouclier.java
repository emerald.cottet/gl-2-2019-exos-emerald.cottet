package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

/**
 * Classe représentant un bouclier.
 * Donne +20 en DEF.
 */
public class Bouclier extends DecoracteurPersonnage {

   public Bouclier(Personnage p) {
      // initialise l'attribut
      super(p);
   }

  // TODO complétez les deux méthodes pour rajouter l'information que ce personnage possède une épée (dans son nom)
  // et augmenter son attaque de 20 points

   @Override
   public String getNom() {
      return personnage.getNom()+" avec bouclier";
   }

   @Override
   public int getDefense() {
      return personnage.getDefense()+20;
   }
}
