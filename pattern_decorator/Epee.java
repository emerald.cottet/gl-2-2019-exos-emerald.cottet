package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

/**
 * Classe représentant une épée.
 * Donne +20 en ATK.
 */
public class Epee extends DecoracteurPersonnage {
   
   public Epee(Personnage p) {
      // initialise l'attribut
      super(p);
   }

  // TODO complétez les deux méthodes pour rajouter l'information que ce personnage possède une épée (dans son nom)
  // et augmenter son attaque de 20 points

   @Override
   public String getNom() {
      return personnage.getNom()+" avec �p�e";
   }

   @Override
   public int getAttaque() {
      return personnage.getAttaque()+20;
   }
}
