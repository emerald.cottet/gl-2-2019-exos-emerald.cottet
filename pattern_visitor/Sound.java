package ex_pattern_visitor;


public class Sound implements Visitor {

	@Override
	public void visit(Cat cat) {
		// print cat sound
	    System.out.println(cat.sound);
		
	}

	@Override
	public void visit(Dog dog) {
		// print cat sound
		System.out.println(dog.sound);
	}

	@Override
	public void visit(Cow cow) {
		// print cat sound
		System.out.println(cow.sound);
	}

}
