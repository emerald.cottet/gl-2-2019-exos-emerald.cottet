package ex_pattern_visitor;


public interface Visitor {
	void visit(Cat cat);
	void visit(Dog dog);
	void visit(Cow cow);
}
