package ex_pattern_visitor;


public interface Visitable {
	void accept(Visitor visitor);
}
