package ex_pattern_visitor;


abstract class Animal implements Visitable {
	protected String sound;
	
	public Animal(String sound) {
		this.sound = sound;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}
	
}
