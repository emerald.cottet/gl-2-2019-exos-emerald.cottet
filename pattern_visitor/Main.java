package ex_pattern_visitor;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
	
	public static void concert(List<Visitable> animals) {
		// create visitor
		// let the visitor visit every animals
	    Visitor v = new Sound();
	    
	    for(Visitable vi : animals) {
	        vi.accept(v);
	    }
	}
	
	public static void main(String[] args) {
		List<Visitable> animals = new ArrayList<Visitable>();
		
		Dog dog = new Dog("Bau");
		Cat cat = new Cat("Miao");
		Cow cow = new Cow("Muuu");
		
		// add animals to list
		animals.add(dog);
		animals.add(cat);
		animals.add(cow);
		
		concert(animals);
		
		
	}

}
