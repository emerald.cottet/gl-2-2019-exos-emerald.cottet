package ex_pattern_visitor;


public class Dog extends Animal {

	public Dog(String sound) {
		/* ... */
	    super(sound);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);		
	}

}
