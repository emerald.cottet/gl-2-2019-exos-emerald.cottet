package ex_pattern_visitor;


public class Cat extends Animal {

	public Cat(String sound) {
		/* ... */
	    super(sound);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
		
	}

}
