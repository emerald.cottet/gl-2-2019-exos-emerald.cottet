package ex_pattern_visitor;


public class Cow extends Animal {

	public Cow(String sound) {
		/* ... */
	    super(sound);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
		
	}

}
