package ex_pattern_adapter;

// Interface décrivant les méthodes que tous les capteurs devraient avoir
public interface ItfCapteur {
    public double mesurerDistanceEnCm();
}
