package ex_pattern_adapter;

import java.util.List;
import java.util.ArrayList;

// Classe contenant le main représentant un robot utilisant différents types de capteurs
public class Robot {
    public static void main(String[] args) {

        // Créer les capteurs
        List<ItfCapteur> capteurs = new ArrayList<>();
        capteurs.add(new CapteurType1());

        // ------- TODO
        // On aimerait ajouter un capteur de type CapteurType2, mais cette classe n'implémente pas l'interface ItfCapteur
        // On ne veut pas modifier la classe CapteurType2, on veut la réutiliser directement
        // 
        // Vous devez créer un adapteur afin de pouvoir utiliser un CapteurType2 comme un ItfCapteur
        // Votre adapteur doit transformer la mesure en pouces vers une mesure en cm

        //capteurs.add(...);
        capteurs.add(new Adapter());

        // Lire les valeurs données par les capteurs
        capteurs.forEach(capteur -> {
            System.out.println(capteur.mesurerDistanceEnCm());
        });
    }
}
