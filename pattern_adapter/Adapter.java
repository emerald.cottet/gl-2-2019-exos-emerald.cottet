package ex_pattern_adapter;

public class Adapter implements ItfCapteur {
    
    CapteurType2 c2 = new CapteurType2();
    
    @Override
    public double mesurerDistanceEnCm() {
        // TODO Auto-generated method stub
        return c2.mesurerDistanceEnPouces()*2.56;   //1cm = 2.56pouces
    }

}
