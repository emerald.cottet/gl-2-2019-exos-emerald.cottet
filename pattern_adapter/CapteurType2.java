package ex_pattern_adapter;

// Classe pour un capteur type 2, pas initialement prévu dans le programme
// on ne veut pas la modifier, c'est pourquoi on ne peut pas la faire implémenter ItfCapteur directement
// on va devoir utiliser le pattern Adapter pour pouvoir l'utiliser dans notre programme
public class CapteurType2 {
    private double val = 100; // valeur "mesurée"

    // Donne la valeur mesurée en pouces
    public double mesurerDistanceEnPouces() {
        return val++;
    }
}
