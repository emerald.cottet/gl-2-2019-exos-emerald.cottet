package ex_pattern_adapter;

// Classe pour un capteur type 1, initialement prévu dans le programme
public class CapteurType1 implements ItfCapteur {
    private double val = 10; // valeur "mesurée"
    
    // Donne la valeur mesurée en cm
    @Override
    public double mesurerDistanceEnCm() {
        return val++;
    }
}
