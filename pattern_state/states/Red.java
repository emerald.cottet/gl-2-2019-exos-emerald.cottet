package states;

public class Red implements State/* TODO implements... */ {
    public void action(TrafficLight c) {
        if (c.time >= 10/* TODO temps >= x */) {
            /*
             * TODO:
             * Reset time
             * Change state
             */
            c.time = 0;
            c.setState(new RedYellow());
            return;
        }
        /*
         * TODO:
         * Increment time
         * Print current state
         */
        c.time++;
        System.out.println("RED");
    }
}
