package states;

public class TrafficLight {
    int time = 0;
    int cycles = 0;
    private State state;

    public TrafficLight() {
        /* TODO Initialize state to Red */
        this.setState(new Red());
    }

    public void setState(State state) {
        this.state = state;
    }

    public void action() {
        state.action(this);
    }

    public int getCycles() {
        return cycles;
    }
}
