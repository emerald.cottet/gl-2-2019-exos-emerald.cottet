package states;

public class Yellow implements State/* TODO implements... */ {
    public void action(TrafficLight c) {
        if (c.time >= 3/* TODO temps >= x */) {
            /*
             * TODO:
             * Reset time
             * Change state
             * !! Increment cycles counter !!
             */
            c.time=0;
            c.setState(new Red());
            c.cycles++;
            return;
        }
        /*
         * TODO:
         * Increment time
         * Print current state
         */
        c.time++;
        System.out.println("YELLOW");
    }
}
