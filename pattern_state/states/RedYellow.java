package states;

public class RedYellow implements State/* TODO implements... */ {
    public void action(TrafficLight c) {
        if (c.time >= 2/* TODO temps >= x */) {
            /*
             * TODO:
             * Reset time
             * Change state
             */
            c.time = 0;
            c.setState(new Green());
            return;
        }
        /*
         * TODO:
         * Increment time
         * Print current state
         */
        c.time++;
        System.out.println("REDYELLOW");
    }
}
