package states;

public class Green implements State /* TODO implements... */ {
    public void action(TrafficLight c) {
        if (c.time >= 20/* TODO temps >= x */) {
            /*
             * TODO:
             * Reset time
             * Change state
             */
            c.time=0;
            c.setState(new Yellow());
            return;
        }
        /*
         * TODO:
         * Increment time
         * Print current state
         */
        c.time++;
        System.out.println("Green");
    }
}
