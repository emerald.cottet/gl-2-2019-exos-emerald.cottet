public interface ICar {
    public boolean startCar();
    public void driveCar();
}
