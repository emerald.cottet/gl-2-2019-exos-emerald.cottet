public class CarEnglishProxy implements ICar {
    private Driver driver;
    private ICar realCar;
    
    public CarEnglishProxy(Driver drivers) {
        // TODO Auto-generated constructor stub
        this.driver = drivers;
        this.realCar = new RealCar();
    }
    
    @Override
    public boolean startCar() {
        // TODO Auto-generated method stub
        return driver.hasKey();
    }
    @Override
    public void driveCar() {
        // TODO Auto-generated method stub
        realCar.startCar();
    }
}
