public class Main {

    public static void main(String[] args) {
        Driver driverUnderAge       = new Driver(13, true, false);
        Driver driverWithoutKey     = new Driver(21, false, true);
        Driver driverWithoutLicense = new Driver(21, true, false);
        Driver swissDriver          = new Driver(21, true, true);
        Driver englishDriver        = new Driver(21, true, true);
        
        Driver[] drivers = {driverUnderAge, driverWithoutKey, driverWithoutLicense, swissDriver, englishDriver};
        
        
        for (int i = 0; i < drivers.length; i++) {
            ICar car;
            if (i != 4) {
                car = new CarSwissProxy(drivers[i]); // TODO
            } else {
                car = new CarEnglishProxy(drivers[i]); // TODO
            }
            
            car.startCar();
            car.driveCar();
        }
    }

}
