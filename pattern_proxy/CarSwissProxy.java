public class CarSwissProxy implements ICar {
    private Driver driver;
    private ICar realCar;
    
    public CarSwissProxy(Driver drivers) {
        // TODO Auto-generated constructor stub
        this.driver = drivers;
        this.realCar = new RealCar();
    }
    
    @Override
    public boolean startCar() {
        // TODO Auto-generated method stub
        if(driver.hasKey() && driver.getAge()>18 && driver.hasLicense()) return true;
        return false;
    }
    @Override
    public void driveCar() {
        // TODO Auto-generated method stub
        if(startCar()) {
            realCar.startCar();
        }
    }
}
