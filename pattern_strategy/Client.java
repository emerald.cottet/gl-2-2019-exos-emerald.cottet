import Strategies.*;
import Client.*;

public class Client {

    public static void main(String[] args) {
        //TODO implement IEatStrategy and it's strategies and FlierClient
        FlierContext flier = new FlierContext(new StandardEatStrategy(), new AlcoholicDrinkStrategy());
        FlierContext vegFlier = new FlierContext(new VegeEatStrategy(), new StandardDrinkStrategy());
        FlierContext noDrinkNoEatFlier = new FlierContext(new NoEatStrategy(), new NoDrinkStrategy());

        //Should output: Standard Flier has a standard lunch and has an alcoholic drink
        System.out.println("Standard Flier " + flier.eats() + " and " + flier.drinks());

        //Should output: Vege Flier has a vegetarian lunch and has a standard drink
        System.out.println("Vege Flier " + vegFlier.eats() + " and " + vegFlier.drinks());

        //Should output: Not hungry and thirsty Flier has no lunch and has no drink
        System.out.println("Not hungry and thirsty Flier " + noDrinkNoEatFlier.eats() + " and " + noDrinkNoEatFlier.drinks());

        //TODO Change strategy for fliers
        // flier.set...
        flier.setEatStrategy(new VegeEatStrategy());
        flier.setDrinkStrategy(new NoDrinkStrategy());

        //Should output: Standard Flier has a vegetarian lunch and has no drink
        System.out.println("Standard Flier " + flier.eats() + " and " + flier.drinks());
    }
}
