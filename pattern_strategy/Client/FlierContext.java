package Client;

import Strategies.IDrinkStrategy;
import Strategies.IEatStrategy;

public class FlierContext {
	//TODO add ohter attribute here
	IDrinkStrategy iDS;
	IEatStrategy iES;

	public FlierContext(Object aIEatStrategy, Object aIDrinkStrategy) { //TODO implement Constructor
		//throw new UnsupportedOperationException();
	    iDS = (IDrinkStrategy) aIDrinkStrategy;
	    iES = (IEatStrategy) aIEatStrategy;
	}

	//TODO implement eat and drink methods
	public String eats(){
		return iES.eats(); /* something.eats */
	}

	public String drinks(){
		return iDS.drinks(); /* something.drinks */
	}

	//TODO implement setters
	public void setEatStrategy(Object aIEatStrategy) {
	    iES = (IEatStrategy) aIEatStrategy;
	}
	
	public void setDrinkStrategy(Object aIDrinkStrategy) {
	    iDS = (IDrinkStrategy) aIDrinkStrategy;
    }

}