package pattern_bridge;

class BridgePatternDemo { 
    public static void main(String[] args) 
    { 
        //creer un vehicule car
        //fabriquer (manufacture) car
        
        //creer un vehicule bicycle
        //fabriquer (manufacture) bicycle
        
        Vehicle v = new Car(new Assembly(), new Production());
        v.manufacture();
        
        Vehicle b = new Bicycle(new Assembly(), new Production());
        b.manufacture();
    } 
} 