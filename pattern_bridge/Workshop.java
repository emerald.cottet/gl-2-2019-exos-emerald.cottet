package pattern_bridge;

interface Workshop
{ 
    abstract public void doWorkshop();  //cette methode fait un print avec l'action du Workshop ex. "Assembled"
    
}