package pattern_bridge;

class Car extends Vehicle/*a completer*/ { 
    public Car(Workshop workShop1, Workshop workShop2) 
    { 
        //a completer
        super(workShop1, workShop2);
    } 
  
    public void manufacture() {
        System.out.println("Car "); 
        //a completer
        //pour fabriquer un vehicule il faut faire les deux workshops
        workShop1.doWorkshop();
        workShop2.doWorkshop();
    }
    
} 