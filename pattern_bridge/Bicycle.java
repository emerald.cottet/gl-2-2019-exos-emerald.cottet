package pattern_bridge;

class Bicycle extends Vehicle/*a completer*/  { 
    public Bicycle(Workshop workShop1, Workshop workShop2) 
    { 
      //a completer
        super(workShop1, workShop2);
    } 
  
    public void manufacture() {
        System.out.println("Bicycle "); 
        //a completer
        //pour fabriquer un vehicule il faut faire les deux workshops
        workShop1.doWorkshop();
        workShop2.doWorkshop();
    }
} 